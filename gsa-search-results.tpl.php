<div class="gsa-results">
  <dl>
  <?php foreach ((array)$results as $row): ?>
    <?php $count++; $first_class = $count == 1 ? 'gsa-first' : ''; ?>
    <?php $last_class = $count == count($results) ? 'gsa-last' : ''; ?>
  
    <dt class="title <?=$first_class?> <?=$last_class?>">
      <a href="<?=$row['url']?>">
        <?=$row['title']?>
      </a>
    </dt>
    <dd class="<?=$first_class?> <?=$last_class?>">
      <div class="teaser"><?=$row['teaser']?></div>
      <div class="bottom">
        <span class="url"><?=$row['url']?></span>
        <span class="crawl_date">- <?=$row['crawl_date']?></span>
      </div>
    </dd>
  <?php endforeach; ?>
  </dl>
</div>