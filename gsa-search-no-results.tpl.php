<div id="no-results-container">
  <div class="no-results-message">
    <div class="title"><?=variable_get('gsa_search_errorcode_1','')?></div> 
    <div class="suggestions">Suggestions:</div>
    <ul>
      <li>Make sure all words are spelled correctly.</li>
      <li>Try different keywords.</li>
      <li>Try more general keywords.</li>
      <li>Try fewer keywords, remove common words such as "the", "and", "or".</li>
      <li>Try the author's name as a keyword.</li>
    </ul>
  </div>
</div>