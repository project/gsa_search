<?php

module_load_include('inc', 'gsa_search');//helper functions
module_load_include('theme.inc', 'gsa_search');//theme functions

/**
 * Implementation of hook_menu()
 *
 */
function gsa_search_menu() {
  $items = array();
  $items['gsa/search'] = array(
    'title' => 'Search Results',
    'page callback' => 'gsa_search_query_gsa',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  
  $items['admin/settings/gsa_search'] = array(
    'title' => t('GSA (Google Search Appliance) Settings'),
    'description' => t('Configuration for the Google Appliance search'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gsa_search_admin_settings'),
    'access arguments' => array('administer gsa search'),
    'type' => MENU_NORMAL_ITEM,
  );
  
  return $items;
}


/**
 * Implementation of hook_perm().
 */
function gsa_search_perm() {
  return array('administer gsa search');
}

/**
 * Query GSA and return 
 *
 * This is a very stripped down way to query GSA. We build a pretty simple query with a few options and hit google and then
 * hand off parsing to a different function. We return the themed output of the entire results page.
 *  
 * Example GET request
 *   http://207.211.15.241/search?site=PetersensHunting&oe=utf8&ie=utf8&getfields=*&client=drupal_frontend&start=0&num=10&filter=0&q=deer&output=xml_no_dtd
 * 
 * @return themed GSA search results page (including pager, results, etcs)
 */
function gsa_search_query_gsa(){

  /**
   * Determine sort parameter (get sort/order from query string)
   */
  if ($_GET['sort']) {
    if ($_GET['order'] == 'desc')
      $sort_param = '&sort=date:D:S:d1';
    elseif ($_GET['order'] == 'asc')
      $sort_param = '&sort=date:A:S:d1';
  }

  /**
   * Determine meta tags filtering
   */
  if ($_GET['filters']) {
    $filter_param = '&requiredfields='.$_GET['filters']; 
  }

  /**
   * Build URL for GSA GET request
   */
  $url = sprintf('http://%s/search?site=%s&oe=utf8&ie=utf8&getfields=*&client=%s&start=%d&num=10&filter=0&q=%s&output=xml_no_dtd%s%s',
    variable_get('gsa_search_host_name',''),
    variable_get('gsa_search_collection',''),
    variable_get('gsa_search_client',''),
    $_GET['page'] * 10,//start
    arg(2),//search terms
    $sort_param, //sort
    $filter_param //meta tag filters
  );

  /**
   * Perform GET request
   */
  $gsa_results_xml = _gsa_search_curl_file_get_contents($url);

  /**
   * Read XML into PHP object 
   */
  if (preg_match('/^<\?xml/', $gsa_results_xml)) {//make sure we're getting back xml
    $xml = simplexml_load_string($gsa_results_xml);
  
    /**
     * Parse results
     */
    $data['results'] = gsa_search_parse_gsa_results($xml);
    
    /**
     * Get total results count
     */
    $data['total_results'] = (integer)$xml->RES->M;//get total results count
    $data['last_result'] = (string)$xml->RES['EN'];//get last result
  }
  else
    $data['gsa_no_response'] = TRUE;
  
  return theme('gsa_search_query_gsa', $data);
}

/**
 * Parse GSA results and return a hash of results
 * 
 * @todo allow overriding of rendering output
 * 
 * @param string $gsa_xml GSA xml output returned from google
 * 
 * @return hash of results (url/title/teaser)
 */
function gsa_search_parse_gsa_results($gsa_xml){
  
  if (!$gsa_xml->RES) return;//no results found
  
  foreach ($gsa_xml->RES->R as $row_xml) {
    //call API hook so other module can extend parsing
    $results[] = module_invoke_all('parse_gsa_result_row', $row_xml);
  }
  
  return $results;
}

/**
 * Implementation of hook_parse_gsa_result_row()
 * 
 * This API function will take a row of GSA xml and parse it. This is
 * an API function so that developers can override and parse additional fields
 * 
 * @param object $row_xml GSA row of XML output mapped as a simpleXML object
 * @return array hash of GSA row data
 *   'url' - page url
 *   'title' - page title
 *   'teaser' - html from page
 *   'crawl_date' - date page was crawled
 */
function gsa_search_parse_gsa_result_row($row_xml){
  
  $url = urldecode($row_xml->U);  
  $_['url'] = substr($url, 0, 80).(strlen($url) > 80 ? '...' : '');
  $_['title'] = strip_tags($row_xml->T);
  $_['teaser'] = decode_entities((string)$row_xml->S); 
  $_['crawl_date'] = (string)$row_xml->FS['VALUE'];
  
  return $_;
}

/**
 * Implementation of hook_block().
 */
function gsa_search_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {

    case 'list':
      $blocks['gsa_search']['info'] = t('GSA Search Block');
      $blocks['gsa_search']['title'] = t('Search'); 

      return $blocks;
      break;

    case 'view':

      switch ($delta) {
        
        case 'gsa_search':
        
          $block['content'] = drupal_get_form('gsa_search_block_form');
          $block['subject'] = t('Search');
          return $block;
          
        break;     
      }

      return $block;
      break;
  }
}

/**
 * GSA Search Block Form
 */
function gsa_search_block_form() {
  $form['keys'] = array(
    '#type' => 'textfield',
    '#title' => '',
    '#default_value' => '',
    '#size' => 20,
    '#maxlength' => 255,
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Search'));

  return $form;
}

/**
 * Implementation of hook_form_submit()
 * 
 * GSA Search Block Form Submit
 * 
 * @param array $form
 * @param array $form_state
 * 
 */
function gsa_search_block_form_submit($form, &$form_state) {
  $form_state['redirect'] = url('gsa/search/', array('absolute' => TRUE)). $form_state['values']['keys'];
}

/**
 * Implementation of hook_admin_settings()
 */
function gsa_search_admin_settings() {
  $form = array();

  // initial required config fields
  $form["config_init"] = array(
    "#title" => t("Initial Configuration"),
    "#type" => "fieldset",
  );

  $form["config_init"]["gsa_search_name"] = array(
    "#type" => "textfield",
    "#size" => 30,
    "#title" => t("Search Name"),
    "#description" => t('The name of this search, to appear as sub-navigation on the search page.'),
    "#default_value" => variable_get('gsa_search_name', 'Google Appliance'),
    "#required" => true,
  );

  $form["config_init"]["gsa_search_host_name"] = array(
    "#type" => "textfield",
    "#size" => 50,
    "#title" => t("Host Name"),
    "#description" => t('Your Google Search Appliance host name or IP address (with http:// or https://), which were assigned when the appliance was set up.<br />You do <b>not</b> need to include "/search" at the end, or a trailing slash, but you should include a port number if needed.<br/> Example: http://mygooglebox.com'),
    "#default_value" => variable_get('gsa_search_host_name', ''),
    "#required" => true,
  );

  $form["config_init"]["gsa_search_collection"] = array(
    "#type" => "textfield",
    "#size" => 50,
    "#title" => t("Collection"),
    "#description" => t('The name of the collection of indexed content to search.'),
    "#default_value" => variable_get('gsa_search_collection', ''),
    "#required" => true,
  );

  $form["config_init"]["gsa_search_client"] = array(
    "#type" => "textfield",
    "#size" => 50,
    "#title" => t("Client"),
    "#description" => t('The name of a valid front-end, defined when you set up the appliance.'),
    "#default_value" => variable_get('gsa_search_client', ''),
    "#required" => true,
  );
  
  $form["config_init"]["gsa_search_timeout"] = array(
    "#type" => "textfield",
    "#size" => 20,
    "#title" => t("Google Search Appliance Timeout"),
    "#description" => t('Length of time to wait for response from Google Search Appliance.'),
    "#default_value" => variable_get('gsa_search_timeout', ''),    
  );

  // error message config
  $form["config_messages"] = array(
    "#title" => t("Error Messages"),
    "#type" => "fieldset",
    "#collapsible" => true,
  );

  $form["config_messages"]["gsa_search_errorcode_1"] = array(
    "#title" => t("No results found"),
    "#type" => "textfield",
    "#size" => 100,
    "#maxlength" => 255,
    "#required" => true,
    "#description" => t('If there are no results for the search criteria.'),
    "#default_value" => variable_get('gsa_search_errorcode_1', 'No results were found that matched your criteria. Please try broadening your search.'),
  );

  $form["config_messages"]["gsa_search_errorcode_neg_100"] = array(
    "#title" => t("Cannot connect to Google Appliance"),
    "#type" => "textfield",
    "#size" => 100,
    "#maxlength" => 255,
    "#required" => true,
    "#description" => t('If the search cannot connect to the Google Appliance server.'),
    "#default_value" => variable_get('gsa_search_errorcode_neg_100', 'We apologize, but the connection to our search engine appears to be down at the moment, please try again later.'),
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_admin_settings_validate()
 * 
 * Validation function, though it's actually getting overridden by the #required fields...
 * 
 * @param array $form
 * @param array $form_state
 */
function gsa_search_admin_settings_validate(&$form, $form_state) {

  if (empty($form['#post']['gsa_search_host_name'])) {
    form_set_error('gsa_search_host_name', t('Please enter your host name or IP address.'));
  }

  if (empty($form['#post']['gsa_search_collection'])) {
    form_set_error('gsa_search_collection', t('Please enter the name of the collection you want to search.'));
  }

  if (empty($form['#post']['gsa_search_client'])) {
    form_set_error('gsa_search_client', t('Please enter name of the client frontend you are searching.'));
  }

  if (empty($form['#post']['gsa_search_name'])) {
    form_set_error('gsa_search_name', t('Please enter the name of this search, to appear as sub-navigation on the search page.'));
  }
}

/**
 * Implementation of hook_admin_settings_submit()
 * 
 * @param array $form
 * @param array $form_state
 * 
 * Submits the admin settings form and saves all the variables.
 */
function gsa_search_admin_settings_submit($form, &$form_state) {
  variable_set('gsa_search_host_name', check_plain($form['gsa_search_host_name']));
  variable_set('gsa_search_collection', check_plain($form['gsa_search_collection']));
  variable_set('gsa_search_client', check_plain($form['gsa_search_client']));
  variable_set('gsa_search_name', check_plain($form['gsa_search_name']));

  // don't run check_plain on these because they can have HTML
  variable_set('gsa_search_errorcode_1', $form['gsa_search_errorcode_1']);
  variable_set('gsa_search_errorcode_neg_100', $form['gsa_search_errorcode_neg_100']);

  drupal_set_message(t('Your settings have been saved.'));
}

/**
 * GSA Search Sorting
 */
function gsa_search_sort_headers(){
  $sort_options = array(
    array(
      'sort' => 'relevance',
      'label' => 'Relevance'      
    ),
    array(
      'sort' => 'date',
      'label' => 'Newest First',
      'order' => 'desc'
    ),
    array(
      'sort' => 'date',
      'label' => 'Oldest First',
      'order' => 'asc'      
    )
  );
  
  $data['sort_options'] = $sort_options;
  
  if ($_GET['filters'])
    $data['filters'] = '&filters='.$_GET['filters'];
  
  return $data;
}