<?php

/**
 * Helper function for querying google
 * 
 * @param string $url URL we are attempting to fetch
 * 
 * @return string curl output
 */
function _gsa_search_curl_file_get_contents($url){
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_VERBOSE, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, variable_get('gsa_search_timeout', 15));//timeout

  return curl_exec($ch);
}


/**
 * GSA Search pager
 * 
 * @param integer $last_result - which number result we are on
 * @param integer $total_results - the total number of results returned by google
 * @param integer $results_per_page
 * 
 * @return array html output broken up for theming
 *  'previous_output' - previous link
 *  'next_output' - next link
 *  'pagination' - numbered links
 */
function gsa_search_pager($last_result = NULL, $total_results = NULL, $results_per_page = 10) {
   if (!$total_results)
     return;

   $q = arg(2);//get search terms
   
   $qs_params_array = array('order', 'sort', 'filters', 'as_q');
   
   /**
    * Look in settings for additional QS parameters to keep track of
    */
   if ($additional_qs_params = variable_get('gsa_search_qs_param', ''))
     $qs_params_array = array_merge($qs_params_array, explode('|', $additional_qs_params));
   
   /**
    * Determine query string params to pass when paging
    */
   foreach ($qs_params_array as $sort)
     if ($_GET[$sort])
       $qs_params .= '&'.$sort.'='.$_GET[$sort];
   
   if ($results_per_page > $total_results)
     $results_per_page = $total_results;
     
   $total_pages = ceil($total_results / $results_per_page);
   
   if ($last_result != $total_results)
     $first = $last_result - $results_per_page + 1;
   else
     $first = ($results_per_page * ($total_pages - 1)) + 1;

   if ($last_result > $results_per_page) {
     if (isset($_GET['page'])) {
       $page1 = $_GET['page'];
       $page1==1 ? $page1=0 : $page1 = $page1-1;
       $url1 = '/gsa/search/'. $q .'?page='. $page1.$qs_params;
     }
     else
       $url1 = '/gsa/search/'. $q .'?page=0'.$qs_params;
     
     $previous_output .= "<a href='". $url1 ."'>Previous Page</a> ";
   }
   
   if ($last_result <= $total_results-1) {
     if (isset($_GET['page'])) {
       $page = $_GET['page'];
       $page==0 ? $page=1 : $page = $page+1;
       $url2 = '/gsa/search/'. $q .'?page='. $page.$qs_params;
     }
     else
       $url2 = '/gsa/search/'. $q .'?page=1'.$qs_params;
      
     $next_output .= " <a href='". $url2 ."'>Next Page</a> ";
   }
   
   // Generate pagination:
   $page = isset($_GET['page']) ? $_GET['page'] : 0;
   $initial_page = $page - 3 > 0 ? ($page - 3) : 1;
   $final_page = $initial_page + 9 < $total_pages ? $initial_page + 9 : $total_pages;
   
   if ($final_page - $initial_page < 9) { $initial_page = $final_page - 9; } 
   if ($initial_page < 1) { $initial_page = 1; }     
   
   for ($i = $initial_page; $i <= $final_page; $i++) {
     if ($i == $final_page)
       $page_class = ' page-final';
     elseif ($i == $initial_page)
       $page_class = ' page-first';
     else
       $page_class = '';
     
     if ($i != ($page + 1))
       $pagination .= '<span class="page'.$page_class.'">' . l($i, 'gsa/search/' . $q, array('query' => 'page=' . ($i - 1).$qs_params)) . '</span> ';
     else
       $pagination .= '<span class="page page-active'.$page_class.'">' . $i . '</span> ';
   }

   if ($i > 2) {//2 means only 1 page
     $data['previous_output'] = $previous_output;
     $data['next_output'] = $next_output;
     $data['pagination'] = $pagination;
   }
   
   return $data;
}