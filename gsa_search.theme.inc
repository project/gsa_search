<?php

/**
 * Implementation of hook_theme()
 */
function gsa_search_theme(){
  return array(
    'gsa_search_query_gsa' => array(
      'arguments' => array(),
    ),
    'gsa_search_results' => array(
      'arguments' => array('results' => NULL),
      'template' => 'gsa-search-results',
    ),
    'gsa_search_no_results' => array(
      'arguments' => array('results' => NULL),
      'template' => 'gsa-search-no-results',
    ),
    'gsa_search_sort_headers' => array(
      'arguments' => array('data' => NULL),
      'template' => 'gsa-search-sort-headers',
    ),
    'gsa_search_pager' => array(
      'arguments' => array(),
    ),
  );
}

/**
 * Theme GSA Search Query GSA
 * 
 * This contains all theme functions so that it can be easily overridden by phptemplate (for rearranging pager/results/etc)
 */
function theme_gsa_search_query_gsa($data){
  
  $output .= '<h2 class="gsa_search_terms">Search results for: '.arg(2).'</h2>';
  
  /**
   * No response from Google Search Appliance / either timeout or server error
   */
  if ($data['gsa_no_response']) {
    $output .= '<div class="gsa_no_response">'.variable_get('gsa_search_errorcode_neg_100','').'</div>';
  }
  /**
   * If results are found
   */
  elseif (count($data['results'])) {    
    $pager = theme('gsa_search_pager', gsa_search_pager($data['last_result'], $data['total_results']));
    
    $output .= sprintf('<div class="gsa_search_items_found">%d Items Found</div>', $data['total_results']);
    
    $output .= theme('gsa_search_sort_headers', gsa_search_sort_headers());
    
    $output .= '<div class="top_gsa_search_pager">'.$pager.'</div>';   
    
    $output .= theme('gsa_search_results', $data['results']);

    $output .= '<div class="bottom_gsa_search_pager">'.$pager.'</div>';
  }
  else { //no results
    $output .= '<div class="gsa_search_items_found">0 Items Found</div>';
    
    $output .= theme('gsa_search_no_results');
  }
  
  $output .= '<div class="gsa_search_powered_by">Powered by Google Search Appliance</div>';
  
  return $output; 
}

/**
 * Theme GSA search page
 * 
 * @param array $data
 *   'previous_output' - previous link
 *   'next_output' - next link
 *   'pagination' - pagination links
 */
function theme_gsa_search_pager($data) {
   $output = '<div class="gsa_search_pager">'.$data['previous_output'] . $data['next_output'] . $data['pagination'].'</div>';
   
   return $output;
}