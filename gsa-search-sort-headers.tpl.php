<div class="gsa-search-sort-headers">

  <span class="sort-by">Sort by:</span>

  <?php foreach ($data['sort_options'] as $sort_opt): ?>
    <?php $count++; $first_class = $count == 1 ? 'sort-field-first' : ''; ?>
    <?php $last_class = $count == count($sort_opt) ? 'sort-field-last' : ''; ?>
    
    <span class="sort-field sort-field-<?=$sort_opt['sort']?> <?=$first_class?> <?=$last_class?>">
      <a href="/gsa/search/<?=arg(2)?>?sort=<?=$sort_opt['sort']?><?=$sort_opt['order'] ? '&order='.$sort_opt['order'] : ''?><?=$data['filters']?>" title="Sort by <?=$sort_opt['label']?>"><?=$sort_opt['label']?></a>
    </span>
  <?php endforeach; ?>
</div>
      